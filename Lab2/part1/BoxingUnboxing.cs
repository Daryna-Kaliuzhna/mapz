﻿namespace lab_mapz_2
{
    internal class BoxingUnboxing
    {
        static void Main(string[] args)
        {

            double box = 5.3;
            object obj = box;

            Console.WriteLine($"Boxing: box = {box} ; obj = {obj}");

            double unbox = (double)obj;

            Console.WriteLine($"Unboxing: unbox = {unbox}");

        }
    }
}