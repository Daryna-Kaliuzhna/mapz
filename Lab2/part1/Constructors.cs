﻿namespace lab_mapz_2
{
    internal class Constructors
    {

        public class Person
        {
            public string Name { get; set; }

            public Person()
            {
                Console.WriteLine("Base class default constructor called.");
            }

            public Person(string name)
            {
                Name = name;
                Console.WriteLine("Base class parameterized constructor called.");
            }

            public virtual void SayHello()
            {
                Console.WriteLine("Hello, I'm Person");
            }
        }

        public class Employee : Person
        {
            public string Position { get; set; }
            public Employee()
            {
                Console.WriteLine("Derived class default constructor called.");
            }
            public Employee(string name, string position) : base(name)
            {
                Position = position;
                Console.WriteLine("Derived class parameterized constructor called with base.");
            }

            public Employee(string name, string position, int age) : this(name, position)
            {
                Console.WriteLine("Derived class parameterized constructor called with this.");
            }

            public override void SayHello()
            {
                Console.WriteLine("Hello, I'm Employee");
            }

        }
        static void Main(string[] args)
        {
            Person person = new Employee();
            person.SayHello();

            Employee employee = new Employee("Bob", "seller", 35);
            employee.SayHello();
        }
    }
}