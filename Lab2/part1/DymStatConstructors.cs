﻿namespace lab_mapz_2
{ 
    internal class DymStatConstructors
    {
        public class Person
        {
            public static string Name;
            public static string FirstName = "Bob";

            public string Surname;
            public string LastName = "Super";

            // Статичний конструктор
            static Person()
            {
                Name = "Static name initialized in static constructor";
                Console.WriteLine("Static constructor called.");
            }

            // Динамічний конструктор
            public Person()
            {
                Surname = "Instance surname initialized in instance constructor";
                Console.WriteLine("Instance constructor called.");
            }


        }
        static void Main(string[] args)
        {
            Person person = new Person();

            Console.WriteLine($"{Person.FirstName}");
            Console.WriteLine($"{Person.Name}");

            Console.WriteLine($"{person.LastName}");
            Console.WriteLine($"{person.Surname}");
        }
    }
}