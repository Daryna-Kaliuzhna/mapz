﻿namespace lab_mapz_2
{
    internal class EnumTypes
    {
        public enum ClothingSize
        {
            XS,
            S,
            M,
            L,
            XL,
            XXL
        }
        static void Main(string[] args)
        {
            ClothingSize firstSize = ClothingSize.S;
            ClothingSize secondSize = ClothingSize.M;


            Console.WriteLine($"Are the first and second sizes the same? {(firstSize == secondSize)}");

            Console.WriteLine($"Are the first and second sizes different? {(firstSize != secondSize)}");


            ClothingSize smallSizes = ClothingSize.XS | ClothingSize.S;
            Console.WriteLine($"Is size S a small size? {(smallSizes & ClothingSize.S) == ClothingSize.S}");


            ClothingSize largeSizes = ClothingSize.XXL;
            Console.WriteLine($"Is size S a large size? {(largeSizes | ClothingSize.S) == ClothingSize.S}");

            ClothingSize size1 = ClothingSize.S;
            ClothingSize size2 = ClothingSize.M;
            ClothingSize size3 = ClothingSize.L;
            ClothingSize size4 = ClothingSize.XS;

            // Перевірка, чи є S, але не L
            bool isSpecificSize = (size1 == ClothingSize.S) ^ (size4 == ClothingSize.L);
            Console.WriteLine($"Is there a specific size? {isSpecificSize}");


            // Перевірка, чи всі розміри є одночасно S, M
            bool allSizesExist = (size1 == ClothingSize.S) && (size2 == ClothingSize.M);
            Console.WriteLine($"Do all sizes exist? {allSizesExist}"); // Виведе: True


            // Перевірка, чи є хоча б один розмір S, L 
            bool anySizeExists = (size4 == ClothingSize.S) || (size3 == ClothingSize.L);
            Console.WriteLine($"Does any size exist? {anySizeExists}");


        }
    }
}