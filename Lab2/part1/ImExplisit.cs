﻿namespace lab_mapz_2
{
    internal class ImExplisit
    {
        public class Weight
        {
            public double _kilo { get; set; }

            public Weight(double kilo)
            {
                this._kilo = kilo;
            }

            // Неявний оператор приведення від типу Weight до типу double
            public static implicit operator double(Weight weight)
            {
                return weight._kilo;
            }

            // Явний оператор приведення від типу double до типу Weight
            public static explicit operator Weight(double kilograms)
            {
                return new Weight(kilograms);
            }
        }

        static void Main(string[] args)
        {

            Weight weight1 = (Weight)75.5; //явне приведення
            Console.WriteLine($"Weight: {weight1._kilo} kilograms");

            Weight weight2 = new Weight(86.4);
            double kilograms = weight2; //неявне приведення
            Console.WriteLine($"Weight: {weight2._kilo} kilograms");

        }
    }
}