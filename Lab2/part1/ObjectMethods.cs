﻿namespace lab_mapz_2
{
    internal class ObjectMethods
    {
        class MyClass
        {
            private string str;

            public MyClass(string str)
            {
                this.str = str;
            }

            public override string ToString()
            {
                return $"My override method ToString() with {str}";
            }

            public override bool Equals(object? obj)
            {
                return this.str.Equals((obj as MyClass).str);

            }

            public override int GetHashCode()
            {
                return base.GetHashCode() * 100;
            }

            protected object MemberwiseClone()
            {
                MyClass clonedObject = (MyClass)base.MemberwiseClone();
                return clonedObject;
            }

        }
    }
}