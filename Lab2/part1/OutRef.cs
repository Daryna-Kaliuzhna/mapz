﻿namespace lab_mapz_2
{
    internal class OutRef
    {

        static void Add(int x, int y, out int result)
        {
            result = x + y;
        }
        static void Swap(ref int x, ref int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }

        static void Print(int x, int y)
        {
            Console.WriteLine($"Numbers: {x}; {y}");
        }

        static void Main(string[] args)
        {
            int a = 2, b = 3, result = 0;
            Add(a, b, out result);
            Console.WriteLine($"{a} + {b} = {result}");


            Console.WriteLine($" a = {a}, b = {b}");
            Swap(ref a, ref b);
            Console.WriteLine($" a = {a}, b = {b}");

            Print(a, b);

        }

    }
}