﻿namespace lab_mapz_2
{
    internal class Program
    {
        public abstract class Person
        {
            private string Name { get; set; } = "";

            internal double Salary { get; set; } = 0;

            public Person(string name)
            {
                Name = name;
            }
            public void SayHello()
            {
                Console.WriteLine($"Hello, my name is {Name}");
            }
            public abstract void Work();
        }
        public interface ISell
        {
            void Sell();
        }

        public interface IPolite
        {
            void bePolite();
        }

        public class Seller : Person, ISell, IPolite
        {
            public Seller(string name) : base(name) { }
            private double productsCount { get; set; }

            public void setProductsCount(double count)
            {
                productsCount = count;
            }

            public double getProductsCount()
            {
                return productsCount;
            }

            public override void Work()
            {
                Console.WriteLine("Seller is working");
            }

            public void Sell()
            {
                productsCount--;
                Console.WriteLine($"The product is sold");
            }

            public void bePolite()
            {
                Console.WriteLine("The seller smiles");
            }

        }

        static void Main(string[] args)
        {

            Seller seller = new Seller("Bob");
            seller.SayHello();
            seller.Work();
            seller.setProductsCount(300);
            seller.Sell();
            Console.WriteLine($"{seller.getProductsCount()} left");
            seller.Salary = 400;
        }
    }
}