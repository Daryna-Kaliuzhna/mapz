﻿namespace lab_mapz_2
{
    internal class fromClassToStruct
    {
        public interface IPerson
        {
            void SayHello();
            void Work();
        }

        public interface ISell
        {
            void Sell();
        }

        public interface IPolite
        {
            void BePolite();
        }

        public struct Person : IPerson
        {
            private string Name;

            public Person(string name)
            {
                Name = name;
            }

            public void SayHello()
            {
                Console.WriteLine($"Hello, my name is {Name}");
            }

            public void Work()
            {
                Console.WriteLine("Person is working");
            }
        }

        public struct Seller : IPerson, ISell, IPolite
        {
            private string Name;
            private double ProductsCount;

            public Seller(string name)
            {
                Name = name;
                ProductsCount = 0;
            }

            public void SayHello()
            {
                Console.WriteLine($"Hello, my name is {Name}");
            }

            public void Work()
            {
                Console.WriteLine("Seller is working");
            }

            public void Sell()
            {
                ProductsCount--;
                Console.WriteLine("The product is sold");
            }

            public void BePolite()
            {
                Console.WriteLine("The seller smiles");
            }
        }

        class Program
        {
            static void Main(string[] args)
            {
                Seller seller = new Seller("Bob");
                seller.SayHello();
                seller.Work();
                seller.Sell();
            }
        }
    }
}