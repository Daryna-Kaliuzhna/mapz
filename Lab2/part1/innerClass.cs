﻿namespace lab_mapz_2
{
    internal class innerClass
    {
        public class Person
        {
            private class Person2
            {
                public void SayHello()
                {
                    Console.WriteLine("Hello, I'm Person2");
                }
            }

            private Person2 per;

            public Person()
            {
                per = new Person2();
            }

            public void SayHi()
            {
                Console.WriteLine("Hi, I'm Person");
                per.SayHello();
            }

        }
        static void Main(string[] args)
        {
            Person p = new Person();
            p.SayHi();
            p.SayHello();

            Person.Person2 per = new Person.Person2();
            per.SayHello();

        }
    }
}