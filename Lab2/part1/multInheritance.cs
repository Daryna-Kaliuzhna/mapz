﻿namespace lab_mapz_2
{
    internal class multInheritance
    {
        public class Person
        {
            private string Name { get; set; } = "";

            internal double Salary { get; set; } = 0;

            public Person(string name)
            {
                Name = name;
            }
            public void SayHello()
            {
                Console.WriteLine($"Hello, my name is {Name}");
            }
        }
        public interface ISell
        {
            void Sell();
        }


        public interface IPolite
        {
            void bePolite();
        }


        public class Seller : Person, ISell, IPolite
        {
            public Seller(string name) : base(name) { }
            private double productsCount { get; set; }

            public void Sell()
            {
                productsCount--;
                Console.WriteLine($"The product is sold");
            }


            public void bePolite()
            {
                Console.WriteLine("The seller smiles");
            }


        }

    }
}