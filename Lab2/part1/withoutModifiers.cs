﻿namespace lab_mapz_2
{
    internal class withoutModifiers
    {
        class Person
        {
            string _name = "";

            Person(string value)
            {
                _name = value;
            }
            string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            class Profession
            {
                string nameProfession = "";
                string experience = "";
            }

            void SayHello()
            {
                Console.WriteLine($"Hello, my name is {_name}");
            }

        }
        struct Work
        {
            string _position;
            int _hours;

            Work(string position, int hours)
            {
                _position = position;
                _hours = hours;
            }

            void doWork()
            {
                Console.WriteLine("Work process");
            }

        }

        interface IWork
        {
            void goLunch();
        }
        static void Main(string[] args)
        {
            Person p = new Person("Booob");
            p._name = "Boob";
            p.SayHello();

            Work w = new Work("teacher", 25);
            w.doWork();


        }
    }
}