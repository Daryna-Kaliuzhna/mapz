﻿using System.Diagnostics;

namespace lab2_2
{
    internal class Program
    {
        public class ClassBoxer<T> where T : struct
        {
            private object _value;
            public T Value
            {
                get { return (T)_value; }
                set { _value = value; }
            }
        }

        public class ValueClass<T> where T : struct
        {
            private T _value;
            public T Value
            {
                get { return _value; }
                set { _value = value; }
            }
        }

        public struct StructWithOneField
        {
            public int Field;
        }

        public struct StructWithFiveFields
        {
            public int Field1;
            public int Field2;
            public int Field3;
            public int Field4;
            public int Field5;
        }

        public struct StructWithTenFields
        {
            public int Field1;
            public int Field2;
            public int Field3;
            public int Field4;
            public int Field5;
            public int Field6;
            public int Field7;
            public int Field8;
            public int Field9;
            public int Field10;
        }

        static void Main(string[] args)
        {
            int IntValue = 5;
            double DoubleValue = 10.5;
            float FloatValue = 6.0f;


            Stopwatch sw = new Stopwatch();

            // INT BOX
            ClassBoxer<int> intBoxed = new ClassBoxer<int>();
            sw.Start();
            for(int i = 0; i < 10000000; i++)
            {
                intBoxed.Value = IntValue;
                int unboxedInt = intBoxed.Value;

                IntValue += i;
            }
            sw.Stop();
            Console.WriteLine($"Int boxing/unboxing time: {sw.ElapsedMilliseconds}");


            // DOUBLE BOX
            ClassBoxer<double> doubleBoxed = new ClassBoxer<double>();
            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                doubleBoxed.Value = DoubleValue;
                double unboxedDouble = doubleBoxed.Value;

                DoubleValue += i;
            }
            sw.Stop();
            Console.WriteLine($"Double boxing/unboxing time: {sw.ElapsedMilliseconds}");



            // FLOAT BOX
            ClassBoxer<float> floatBoxed = new ClassBoxer<float>();
            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                floatBoxed.Value = FloatValue;
                float unboxedFloat = floatBoxed.Value;

                FloatValue += i;
            }
            sw.Stop();
            Console.WriteLine($"Float boxing/unboxing time: {sw.ElapsedMilliseconds}");



            // INT
            ValueClass<int> IntOrdinary = new ValueClass<int>();
            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                IntOrdinary.Value = IntValue;
                int value = IntOrdinary.Value;

                IntValue += i;
            }
            sw.Stop();
            Console.WriteLine($"\nInt ordinary assignment time: {sw.ElapsedMilliseconds}");


            // DOUBLE
            ValueClass<double> DoubleOrdinary = new ValueClass<double>();
            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                DoubleOrdinary.Value = DoubleValue;
                double value = DoubleOrdinary.Value;

                DoubleValue += i;
            }
            sw.Stop();
            Console.WriteLine($"Double ordinary assignment time: {sw.ElapsedMilliseconds}");


            // FLOAT
            ValueClass<float> FloatOrdinary = new ValueClass<float>();
            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                FloatOrdinary.Value = FloatValue;
                float value = FloatOrdinary.Value;

                FloatValue += i;
            }
            sw.Stop();
            Console.WriteLine($"Float ordinary assignment time: {sw.ElapsedMilliseconds}");


            // STRUCT 1 FIELD

            StructWithOneField oneField = new StructWithOneField();
            ClassBoxer<StructWithOneField> structOne = new ClassBoxer<StructWithOneField>();
            StructWithOneField unboxedOne;

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                structOne.Value = oneField;
                unboxedOne = structOne.Value;
            }
            sw.Stop();
            Console.WriteLine($"\nStruct with one field time: {sw.ElapsedMilliseconds}");


            // STRUCT 5 FIELDS

            StructWithFiveFields fiveFields = new StructWithFiveFields();
            ClassBoxer<StructWithFiveFields> structFive = new ClassBoxer<StructWithFiveFields>();
            StructWithFiveFields unboxedFive;

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                structFive.Value = fiveFields;
                unboxedFive = structFive.Value;
            }
            sw.Stop();
            Console.WriteLine($"Struct with five fields time: {sw.ElapsedMilliseconds}");


            // STRUCT 10 FIELDS

            StructWithTenFields tenFields = new StructWithTenFields();
            ClassBoxer<StructWithTenFields> structTen = new ClassBoxer<StructWithTenFields>();
            StructWithTenFields unboxedTen;

            sw = Stopwatch.StartNew();
            for (int i = 0; i < 10000000; i++)
            {
                structTen.Value = tenFields;
                unboxedTen = structTen.Value;
            }
            sw.Stop();
            Console.WriteLine($"Struct with ten fields time: {sw.ElapsedMilliseconds}");

        }
    }
}