﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;

namespace lab3
{
    internal class Data
    {
        public static List<Hotel> GenerateHotels()
        {
            var hotels = new List<Hotel>
            {
                new Hotel("Lviv"),
                new Hotel("Fairmont Grand Hotel"),
                new Hotel("Hilton")
            };

            return hotels; 
        }

        public static List<Room> GenerateRooms(List<Hotel> hotels)
        {
            var rooms = new List<Room>
            {
                new Room(1, "single standard"),
                new Room(2, "double standard"),
                new Room(3, "deluxe"),
                new Room(4, "luxe")

            };


            foreach (var hotel in hotels)
            {
                foreach(var room in rooms)
                {
                    hotel.Rooms.Add(room);
                }
            }

            return rooms;
        }

        public static void GenerateFurniture(List<Room> rooms)
        {
            var furnitureSet1 = new List<Furniture>
            {
                new Furniture("bed", "wood", true, 1500),
                new Furniture("table", "wood", true, 1000),
                new Furniture("wardrobe", "wood", false, 2500),
                new Furniture("chair", "leather", false, 1450)
            };

            var furnitureSet2 = new List<Furniture>
            {
                new Furniture("bed", "wood", true, 1500),
                new Furniture("table", "wood", true, 1000),
                new Furniture("desk", "metal", true, 800),
                new Furniture("bookshelf", "wood", true, 1200),
                new Furniture("lamp", "plastic", true, 300),
                new Furniture("lamp", "plastic", true, 300)
            };

            var furnitureSet3 = new List<Furniture>
            {
                new Furniture("bed", "wood", true, 1500),
                new Furniture("sofa", "fabric", true, 2000),
                new Furniture("coffee table", "glass", true, 700),
                new Furniture("tv stand", "wood", false, 1500),
                new Furniture("bookshelf", "wood", true, 1200),
            };

            var furnitureSet4 = new List<Furniture>
            {
                new Furniture("bed", "wood", true, 1500),
                new Furniture("nightstand", "wood", true, 500),
                new Furniture("dresser", "wood", true, 1200),
                new Furniture("table", "wood", true, 1000),
                new Furniture("wardrobe", "wood", false, 2500),
                new Furniture("chair", "leather", false, 1450),
                new Furniture("bookshelf", "wood", true, 1200),
            };

            
            foreach (var item in furnitureSet1)
            {
                rooms[0].Furniture.Add(item);
            }
            foreach (var item in furnitureSet2)
            {
                rooms[1].Furniture.Add(item);
            }
            foreach (var item in furnitureSet3)
            {
                rooms[2].Furniture.Add(item);
            }
            foreach (var item in furnitureSet4)
            {
                rooms[3].Furniture.Add(item);
            }
        }
    }
}
