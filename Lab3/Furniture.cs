﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    public class Furniture
    {
        public string Name { get; set; }
        public string Material { get; set; }
        public bool IsNew { get; set; }
        public double Price { get; set; }

        public Furniture(string name, string material, bool isNew, double price)
        {
            this.Name = name;
            this.Material = material;
            this.IsNew = isNew;
            this.Price = price;
        }
    }
}
