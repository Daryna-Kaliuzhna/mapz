﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace lab3
{
    public class Hotel
    {
        public string Name { get; set; }
        public List<Room> Rooms { get; set; }

        public Hotel(string name)
        {
            this.Name = name;
            Rooms = new List<Room>();
        }

    }
}
