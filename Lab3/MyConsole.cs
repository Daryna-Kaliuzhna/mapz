﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    internal class MyConsole
    {
        static void Main(string[] args)
        {
            List<Hotel> hotels = Data.GenerateHotels();
            List<Room> rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);

            foreach (var hotel in hotels)
            {
                Console.WriteLine($"Hotel: {hotel.Name} \n"); 

                foreach (var room in hotel.Rooms)
                {
                    Console.WriteLine(String.Format("Room ID: {0}, Type: {1}", room.ID, room.Type));

                    room.Furniture.ForEach(furniture =>
                    {
                        Console.WriteLine($"Furniture: Name - {furniture.Name}, Material - {furniture.Material}, IsNew - {furniture.IsNew}, Price - {furniture.Price}");
                    });

                    Console.WriteLine("\n");
                }
                Console.WriteLine("\n");
            }

           
        }
    }
}
