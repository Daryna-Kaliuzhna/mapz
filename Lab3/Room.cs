﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    public class Room
    {

        public int ID;

        public string Type;
        public List<Furniture> Furniture { get; set; }

        public Room(int id, string type)
        {
            this.ID = id;
            this.Type = type;
            Furniture = new List<Furniture>();
        }
    }
}
