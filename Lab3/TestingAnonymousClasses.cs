﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingAnonymousClasses
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }

        [Test]
        public void AnonymousClass_RoomsType()
        {
            var foundRooms = rooms.Where(room => new {Type = room.Type }.Type == "deluxe");

            Assert.That(foundRooms.Count(), Is.EqualTo(1));
            Assert.That(foundRooms.All(room=>room.Type == "deluxe"), Is.True);
        }

        [Test]
        public void AnonymousClass_FurnitureType()
        {
            var foundFurniture = rooms.SelectMany(room => room.Furniture).Where(furniture => new { Material = furniture.Material }.Material == "wood");
          
            Assert.That(foundFurniture, Is.Not.Empty);
            Assert.That(foundFurniture.All(furniture => furniture.Material == "wood"), Is.True);

        }

        [Test]
        public void AnonymousClass_GroupByNameAndPrice()
        {
            var groupedByNameAndPrice = rooms.SelectMany(room => room.Furniture).GroupBy(furniture => new { furniture.Name, furniture.Price});

            Assert.That(groupedByNameAndPrice.Any(furniture => furniture.Key.Name == "bed" && furniture.Key.Price == 1500 && furniture.Count() == 4));
        }
    }
}
