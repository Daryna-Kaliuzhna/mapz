﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingConversion
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }
        [Test]
        public void Conversion_HotelsToArray()
        {
            var array = hotels.ToArray();

            Assert.That(hotels.Count, Is.EqualTo(array.Length));
            Assert.That(hotels[1].Name, Is.EqualTo(array[1].Name));
            Assert.That(hotels[2].Rooms, Is.EqualTo(array[2].Rooms));

        }

        [Test]
        public void Conversion_RoomsToArray() 
        { 
            var array = rooms.ToArray();    

            Assert.That(rooms.Count, Is.EqualTo(array.Length));
            Assert.That(rooms[1].ID, Is.EqualTo(array[1].ID));
            Assert.That(rooms[2].Furniture, Is.EqualTo(array[2].Furniture));
        }

        [Test]
        public void Conversion_FurnitureToArray()
        {
            var array = rooms.FirstOrDefault().Furniture.ToArray();

            Assert.That(rooms.FirstOrDefault().Furniture.Count, Is.EqualTo(array.Length));
            Assert.That(rooms.FirstOrDefault().Furniture[0].Price, Is.EqualTo(array[0].Price));
            Assert.That(rooms.FirstOrDefault().Furniture[1].Material, Is.EqualTo(array[1].Material));
        }
    }
}
