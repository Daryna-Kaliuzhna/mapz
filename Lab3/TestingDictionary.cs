﻿using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingDictionary
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }

        [Test]
        public void Dictionary_RoomsPerHotel()
        {
            var hotelDictionary = hotels.ToDictionary(hotel => hotel.Name, hotel=>hotel.Rooms.Count);

           Assert.That(hotelDictionary.Count, Is.EqualTo(3));
           Assert.That(hotelDictionary.Keys.FirstOrDefault(), Is.EqualTo("Lviv"));
           Assert.That(hotelDictionary.Values.FirstOrDefault(), Is.EqualTo(4));
        }

        [Test]
        public void Dictionary_IdRoomsTypeDictionary()
        {
            var roomsDictionary = rooms.ToDictionary(room => room.ID, room => room.Type);

            Assert.That(roomsDictionary.Count, Is.EqualTo(4));
            Assert.That(roomsDictionary.Keys.FirstOrDefault(), Is.EqualTo(1));
            Assert.That(roomsDictionary.Values.FirstOrDefault(), Is.EqualTo("single standard"));
        }
        [Test]
        public void Dictionary_FurniturePriceDictionary()
        {
            var selectedRoom = rooms.FirstOrDefault();

            var furnitureDictionary = selectedRoom.Furniture.ToDictionary(furniture => furniture.Name, furniture => furniture.Price);

            Assert.That(furnitureDictionary.Count, Is.EqualTo(selectedRoom.Furniture.Count));
            Assert.That(furnitureDictionary.Keys.FirstOrDefault(), Is.EqualTo("bed"));
            Assert.That(furnitureDictionary.Values.FirstOrDefault(), Is.EqualTo(1500));
          
        }

        [Test]
        public void Dictionary_RoomFurnitureList()
        {
            var furnitureDictionary = rooms.ToDictionary(room=> room.ID, room => room.Furniture);
         
            Assert.That(furnitureDictionary, Is.Not.Empty);
            Assert.That(furnitureDictionary.Keys, Is.EquivalentTo(rooms.Select(room => room.ID)));
            foreach (var kvp in furnitureDictionary)
            {
                Assert.That(kvp.Value, Is.InstanceOf<List<Furniture>>());
            }

        }
    }
}
