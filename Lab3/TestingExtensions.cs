﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;

namespace lab3_unit_tests.Tests
{
    internal class TestingExtensions
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }

        [Test]
        public void Extension_AveragePrice()
        {
            var selectedFurniture = rooms.SelectMany(room => room.Furniture);
            var expectedAveragePrice = rooms.SelectMany(room => room.Furniture).Average(furniture => furniture.Price);

            var averagePrice = selectedFurniture.AveragePrice();

            Assert.That(averagePrice, Is.EqualTo(expectedAveragePrice));

        }

        [Test]
        public void Extension_MarkAsOld()
        {
            var selectedFurniture = rooms.SelectMany(room => room.Furniture);
            var newFurniture = selectedFurniture.Where(furniture => furniture.IsNew == true).ToList();

            newFurniture.ForEach(furniture => furniture.MarkAsOld());

            Assert.That(selectedFurniture.Any(furniture => furniture.IsNew), Is.EqualTo(false));
        }

        [Test]
        public void Extension_GetTotalFurnitureCost() 
        {
            var selectedRoom = rooms.FirstOrDefault();
            var totalCost = selectedRoom.GetTotalFurnitureCost();

            var expectedTotalCost = selectedRoom.Furniture.Sum(furniture => furniture.Price);

            Assert.That(totalCost, Is.EqualTo(expectedTotalCost));        
        }
    }
}
