﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingListStructures
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }

        [Test]
        public void SortedList_RoomsByFurnitureCount()
        {
          
            var sortedRooms = new SortedList<int, List<Room>>();

            foreach (var room in rooms)
            {
                int furnitureCount = room.Furniture.Count;
                if (!sortedRooms.ContainsKey(furnitureCount))
                {
                    sortedRooms.Add(furnitureCount, new List<Room>());
                }
                sortedRooms[furnitureCount].Add(room);
            }

            Assert.That(sortedRooms, Is.Not.Empty);

            foreach (var kvp in sortedRooms)
            {
                Assert.That(kvp.Value, Is.Not.Empty);
                Assert.That(kvp.Value.All(room => room.Furniture.Count == kvp.Key), Is.True);
            }
        }



        [Test]
        public void Queue_NewArrivals()
        {
            
            var newArrivalsQueue = new Queue<Room>();
         
            foreach (var room in rooms)
            {
                newArrivalsQueue.Enqueue(room);
            }

            Assert.That(newArrivalsQueue, Is.Not.Empty);
            Assert.That(newArrivalsQueue.Count, Is.EqualTo(rooms.Count));
        }


    }
}
