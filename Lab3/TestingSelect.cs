﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingSelect
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }

        [Test]
        public void Select_HotelsName()
        {
            var selectedHotels = hotels.Select(hotel => hotel.Name);   
            Assert.That(selectedHotels, Is.Not.Empty);
        }

        [Test]
        public void Select_RoomsName()
        {
            var selectedRooms = rooms.Select(room => room.ID);
            Assert.That(selectedRooms, Is.Not.Empty);
        }

        [Test]

        public void Select_FurnitureName()
        {
            var furnitureNames = rooms.SelectMany(room => room.Furniture).Select(furniture => furniture.Name); 
            Assert.That(furnitureNames, Is.Not.Empty); 
        }

        [Test]
        public void Select_RoomType()
        {
            var firstHotelRoomTypes = hotels.First().Rooms.Select(room => room.Type).ToList();
            var expectedRoomTypes = rooms.Select(room => room.Type).ToList();
            Assert.That(firstHotelRoomTypes, Is.EquivalentTo(expectedRoomTypes));
        }

        [Test]
        public void Select_RoomsCountEqualHotelRooms()
        {
            var totalRoomCount = hotels.Select(hotel => hotel.Rooms).First();
            Assert.That(totalRoomCount.Count, Is.EqualTo(rooms.Count));
        }

        [Test]
        public void Select_NewFurniture()
        {
            var allNewFurniture = rooms.SelectMany(room => room.Furniture).All(furniture => furniture.IsNew);
            Assert.That(allNewFurniture, Is.False);
        }
        [Test]
        public void Select_AllFurnitureHaveSameMaterial()
        {
            foreach (var room in rooms)
            {
                var distinctMaterials = room.Furniture.Select(furniture => furniture.Material).Distinct().Count();
                Assert.That(distinctMaterials, Is.Not.EqualTo(1));
            }
        }
        [Test]
        public void Select_CheaperFurniture()
        {
            var MaxPrice = 3000;

            var cheaperFurniture = rooms.SelectMany(room => room.Furniture).All(furniture => furniture.Price < MaxPrice);
            Assert.That(cheaperFurniture, Is.True);
        }
    }
}
