﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;


namespace lab3_unit_tests.Tests
{
    internal class TestingSort
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }
        [Test]
        public void Sort_HotelsByName()
        {
           
            var sortedHotels = hotels.OrderBy(hotel => hotel.Name).ToList();

            Assert.That(sortedHotels, Is.Not.Empty);

            for (int i = 0; i < sortedHotels.Count - 1; i++)
            {
                Assert.That(sortedHotels[i].Name.CompareTo(sortedHotels[i + 1].Name), Is.LessThanOrEqualTo(0));
            }
        }

        [Test]
        public void Sort_HotelsByNumberOfRooms()
        {
           
            var sortedHotels = hotels.OrderBy(hotel => hotel.Rooms.Count).ToList();

            Assert.That(sortedHotels, Is.Not.Empty);

            for (int i = 0; i < sortedHotels.Count - 1; i++)
            {
                Assert.That(sortedHotels[i].Rooms.Count, Is.LessThanOrEqualTo(sortedHotels[i + 1].Rooms.Count));
            }
        }

        [Test]
        public void Sort_RoomsByType()
        {
            foreach (var hotel in hotels)
            {
                var sortedRooms = hotel.Rooms.OrderBy(room => room.Type).ToList();

                Assert.That(sortedRooms, Is.Not.Empty);

                for (int i = 0; i < sortedRooms.Count - 1; i++)
                {
                    Assert.That(sortedRooms[i].Type.CompareTo(sortedRooms[i + 1].Type), Is.LessThanOrEqualTo(0));
                }
            }
        }

        [Test]
        public void Sort_RoomsByFurnitureCount()
        {
            foreach (var hotel in hotels)
            {
                var sortedRooms = hotel.Rooms.OrderBy(room => room.Furniture.Count).ToList();

                Assert.That(sortedRooms, Is.Not.Empty);

                for (int i = 0; i < sortedRooms.Count - 1; i++)
                {
                    Assert.That(sortedRooms[i].Furniture.Count, Is.LessThanOrEqualTo(sortedRooms[i + 1].Furniture.Count));
                }
            }
        }
    }
}
