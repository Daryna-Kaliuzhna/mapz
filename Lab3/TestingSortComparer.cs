﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingSortComparer
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }
        [Test]
        public void Comparer_SortHotelByName()
        {
            var expectedSortedHotels = new List<string> { "Fairmont Grand Hotel", "Hilton", "Lviv" };
            hotels.Sort(new HotelsNameComparer());

            Assert.That(hotels.Select(hotel => hotel.Name).ToList(), Is.EqualTo(expectedSortedHotels));
        }

        [Test]
        public void Comparer_SortFurnitureByName() 
        {
            var expectedSortedFurniture = new List<string> { "bed", "chair", "table", "wardrobe" };
            var selectedRoom = rooms.FirstOrDefault();
            var selectedFurniture = selectedRoom.Furniture;
            selectedFurniture.Sort(new FurnitureNameComparer());

            Assert.That(selectedFurniture.Select(furniture => furniture.Name).ToList(), Is.EqualTo(expectedSortedFurniture));

        }
        [Test]
        public void Comparer_SortFurnitureByPrice()
        {
            var expectedSortedFurniture = new List<double> { 1000, 1450, 1500, 2500 };
            var selectedRoom = rooms.FirstOrDefault();
            var selectedFurniture = selectedRoom.Furniture;
            selectedFurniture.Sort(new FurniturePriceComparer());

            Assert.That(selectedFurniture.Select(furniture => furniture.Price).ToList(), Is.EqualTo(expectedSortedFurniture));

        }
        [Test]
        public void Comparer_SortRoomsByFurnitureCount()
        {
            var expectedSortedRoomsType = new List<string> { "single standard", "deluxe", "double standard", "luxe" };

            rooms.Sort(new RoomFurnitureCountComparer());

            Assert.That(rooms.Select(room => room.Type).ToList(), Is.EqualTo(expectedSortedRoomsType));
        }
    }
}
