﻿using lab3;
using lab3_unit_tests.Additional;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace lab3_unit_tests.Tests
{
    internal class TestingWhere
    {
        private List<Hotel> hotels;
        private List<Room> rooms;

        [SetUp]
        public void Setup()
        {
            hotels = Data.GenerateHotels();
            rooms = Data.GenerateRooms(hotels);
            Data.GenerateFurniture(rooms);
        }
        [Test]
        public void Where_FindHotelsByName() {

            var foundName = hotels.Where(hotel => hotel.Name == "Lviv");

            Assert.That(foundName.Count, Is.EqualTo(1));
            Assert.That(foundName.First().Name, Is.EqualTo("Lviv"));
        }

        [Test]
        public void Where_FindRoomsByType()
        {
            var expectedType = "deluxe";
            var foundRooms = rooms.Where(room => room.Type == expectedType);

            Assert.That(foundRooms.Count, Is.EqualTo(1));
            Assert.That(foundRooms.First().Type, Is.EqualTo(expectedType));

        }

        [Test]
        public void Where_FindRoomsById()
        {
            var expectedId = 1;
            var foundRooms = rooms.Where(room=>room.ID == expectedId);

            Assert.That(foundRooms.Count, Is.EqualTo(1));
            Assert.That(foundRooms.First().ID, Is.EqualTo(expectedId));
        }

        [Test]  
        public void Where_FindFurnitureByName()
        {
            var expectedFurniture = "table";
            var foundFurniture = rooms.SelectMany(room => room.Furniture).Where(furniture => furniture.Name == expectedFurniture);

            Assert.That(foundFurniture, Is.Not.Empty);
            Assert.That(foundFurniture.First().Name, Is.EqualTo(expectedFurniture));
        }

        [Test]
        public void Where_FindFurnitureByMaterial()
        {
            var expectedMaterial = "wood";
            var foundFurniture = rooms.SelectMany(room=> room.Furniture).Where(furniture => furniture.Material == expectedMaterial);

            Assert.That(foundFurniture, Is.Not.Empty);
            Assert.That(foundFurniture.First().Material, Is.EqualTo(expectedMaterial));
        }

        [Test]
        public void Where_FindFurnitureByState()
        {
            var expectedState = true;
            var foundFurniture = rooms.SelectMany(room=>room.Furniture).Where(furniture => furniture.IsNew == expectedState);

            Assert.That(foundFurniture, Is.Not.Empty);
            Assert.That(foundFurniture.First().IsNew, Is.EqualTo(expectedState));
        }

        [Test]
        public void Where_FindFurnitureByPrice()
        {
            var expectedPrice = 1500;
            var foundFurniture = rooms.SelectMany(room=>room.Furniture).Where(furniture => furniture.Price == expectedPrice);

            Assert.That(foundFurniture, Is.Not.Empty);
            Assert.That(foundFurniture.First().Price, Is.EqualTo(expectedPrice));
        }
    }
}
