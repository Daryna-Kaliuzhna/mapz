﻿using Hotel.Models.Builders;
using Hotel.Models;
using Microsoft.AspNetCore.Mvc;
using Hotel.Models.AbstractFactory;

namespace Hotel.Controllers
{
    public class FurnitureController : Controller
    {
        private readonly ILogger<FurnitureController> _logger;
        public FurnitureController(ILogger<FurnitureController> logger)
        {
            _logger = logger;
        }

        public Hotel.Models.Hotel hotel = Hotel.Models.Hotel.GetInstance();

        public IActionResult FurnitureInfo(string shop)
        {
            List<Furniture> model = new List<Furniture>();

            Furniture chair;
            Furniture table;
            Furniture sofa;
            Furniture bed;

            FurnitureFactory furnitureFactory = new EconomyFurnitureFactory();
             

            switch (shop)
            {
                case "economy":
                    furnitureFactory = new EconomyFurnitureFactory();
                    break;

                case "standard":
                    furnitureFactory = new StandardFurnitureFactory();
                    break;

                case "luxe":
                    furnitureFactory = new LuxeFurnitureFactory();
                    break;

                default:

                    break;
            }

            chair = furnitureFactory.CreateChair();
            table = furnitureFactory.CreateTable();
            sofa = furnitureFactory.CreateSofa();
            bed = furnitureFactory.CreateBed();

            model.Add(chair);
            model.Add(table);
            model.Add(sofa);
            model.Add(bed);

            return View("FurnitureInfo", model);
        }
    }
}
