﻿using Hotel.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Hotel.Models.Builders;

namespace Hotel.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        public IActionResult AdditionalInfo() { 
            return View();
        }

        public Hotel.Models.Hotel hotel = Hotel.Models.Hotel.GetInstance();
        public IActionResult AddFeature(string featureType)
        {
            string featureToAdd;

            switch (featureType.ToLower())
            {
                case "swimmingpool":
                    featureToAdd = "Swimming pool";
                    break;
                case "gym":
                    featureToAdd = "Gym";
                    break;
                case "spa":
                    featureToAdd = "Spa";
                    break;
                default:
                    return RedirectToAction("Error");
            }

            if (hotel.Features.Contains(featureToAdd))
            {
                return RedirectToAction("RoomsInfo", "Rooms", hotel.Rooms);
            }
            else
            {
                hotel.AddAdditionalFeature(featureToAdd);
                return RedirectToAction("RoomsInfo", "Rooms", hotel.Rooms);
            }
        }

    }
}