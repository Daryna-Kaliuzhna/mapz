﻿using Hotel.Models;
using Hotel.Models.Facade;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace Hotel.Controllers
{
    public class ReservationController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReservationInfo() 
        {
            return View();
        }

        public Hotel.Models.Hotel hotel = Hotel.Models.Hotel.GetInstance();
        public IActionResult GuestInfo(string guestName, string roomType, string idDocument, DateTime startDate, DateTime endDate)
        {
            HotelManagementSystem manager = new HotelManagementSystem();

            List<Room> availableRooms = hotel.Rooms.Where(r => (r.Type == roomType) && !r.IsAvailable).ToList();

            if (availableRooms.Any())
            {
                Room selectedRoom = availableRooms.First();

                manager.BookRoom(selectedRoom.ID);

                manager.RegisterGuest(guestName, idDocument);

                manager.ProvideRoomKey(selectedRoom.ID, idDocument, roomType, startDate, endDate);

                return View("ReservationInfo", hotel.Guests);
            }
            return RedirectToAction("ReservationInfo");
        }
    }
}
