﻿using Hotel.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Hotel.Models.Builders;
using Hotel.Models.Decorator;
using Hotel.Models.Strategy;
using Hotel.Models.State;

namespace Hotel.Controllers
{
    public class RoomsController : Controller
    {
        private readonly ILogger<RoomsController> _logger;
        public RoomsController(ILogger<RoomsController> logger)
        {
            _logger = logger;
        }
        public Hotel.Models.Hotel hotel = Hotel.Models.Hotel.GetInstance();
        public IActionResult RoomsInfo(string viewMode)
        {
            ViewBag.ViewMode = viewMode;
             
            if(hotel.Rooms.Count == 0)
            {
                int idCounter = 1;

                var economyBuilder = new EconomyBuilder();
                var director = new BuilderDirector(economyBuilder);

                director.ConstructRoom();
                Room economyRoom1 = economyBuilder.GetRoom();
                economyRoom1.ID = idCounter++;
                Room economyRoom2 = economyBuilder.GetRoom();
                economyRoom2.ID = idCounter++;

                hotel.Rooms.Add(economyRoom1);
                hotel.Rooms.Add(economyRoom2);

                var standardBuilder = new StandardBuilder();
                director = new BuilderDirector(standardBuilder);

                director.ConstructRoom();
                Room standardRoom = standardBuilder.GetRoom();
                standardRoom.ID = idCounter++;

                hotel.Rooms.Add(standardRoom);

                var luxeBuilder = new LuxeBuilder();
                director = new BuilderDirector(luxeBuilder);

                director.ConstructRoom();
                Room luxeRoom = luxeBuilder.GetRoom();
                luxeRoom.ID = idCounter++;

                hotel.Rooms.Add(luxeRoom);

                foreach (var room in hotel.Rooms)
                {
                    hotel.AddRoomObserver(room);
                }

            }

            return View(hotel.Rooms);
        }
        public IActionResult EditRooms()
        {
            return View();
        }

        public IActionResult AddRoom(string roomType, bool goodView, bool wifi, bool breakfast)
        {
            int idCounter = hotel.Rooms.Count + 1; 

            IBuilder roomBuilder = null;
            switch (roomType.ToLower())
            {
                case "economy":
                    roomBuilder = new EconomyBuilder();
                    break;
                case "standard":
                    roomBuilder = new StandardBuilder();
                    break;
                case "luxe":
                    roomBuilder = new LuxeBuilder();
                    break;
                default:
                    return RedirectToAction("Error");
            }

            var director = new BuilderDirector(roomBuilder);

            director.ConstructRoom();
          
            Room newRoom = roomBuilder.GetRoom();
            newRoom.ID = idCounter;

            if(goodView == true)
            {
                var decorator = new GoodViewDecorator(newRoom);
                decorator.SetPrice();
                decorator.SetDescription();
            }
            if(wifi == true)
            {
                var decorator = new WiFiDecorator(newRoom);
                decorator.SetPrice();
                decorator.SetDescription();
            }
            if(breakfast == true)
            {
                var decorator = new BreakFastDecorator(newRoom);
                decorator.SetPrice();
                decorator.SetDescription();
            }

            hotel.Rooms.Add(newRoom);
            hotel.AddRoomObserver(newRoom);
            foreach (var feature in hotel.Features)
            {
                newRoom.AddFeatureToRoom(feature);
            }
            return View("RoomsInfo", hotel.Rooms);
        }
        public IActionResult SortRooms(string sortType)
        {
            RoomSortManager manager = new RoomSortManager(new SortByEconomyStandardLuxuryStrategy());

            switch (sortType.ToLower())
            {
                case "priceascending":
                    manager = new RoomSortManager(new SortByPriceAscendingStrategy());
                    break;
                case "pricedescending":
                    manager = new RoomSortManager(new SortByPriceDescendingStrategy());
                    break;
                case "typeluxurystandardeconomy":
                    manager = new RoomSortManager(new SortByLuxuryStandardEconomyStrategy());
                    break;
                case "typeeconomystandardluxury":
                    manager = new RoomSortManager(new SortByEconomyStandardLuxuryStrategy());
                    break;
                default:
                    return RedirectToAction("Error");
            }
            manager.SortRooms(hotel.Rooms);

            return View("RoomsInfo", hotel.Rooms);
        }

        public IActionResult ChangeState(string state, int roomID)
        {
            Room room = hotel.Rooms.FirstOrDefault(r => r.ID == roomID);

            if(room != null) {
                switch (state)
                {
                    case "repairing":
                        room.UpdateState(new RepairState());
                        room.ChangeState();
                        break;
                    case "cleaning":
                        room.UpdateState(new CleaningState());
                        room.ChangeState();
                        break;
                    default:
                        break;
                }
            }

            return View("RoomsInfo", hotel.Rooms);
        }
    }
}
