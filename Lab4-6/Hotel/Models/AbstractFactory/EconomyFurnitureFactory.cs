﻿using Hotel.Models.EconomyFurniture;

namespace Hotel.Models.AbstractFactory
{
    public class EconomyFurnitureFactory : FurnitureFactory
    {
        public override Furniture CreateChair()
        {
            return new EconomyChair();
        }
        public override Furniture CreateTable()
        {
            return new EconomyTable();
        }
        public override Furniture CreateSofa()
        {
            return new EconomySofa();
        }
        public override Furniture CreateBed()
        {
            return new EconomyBed();
        }
    }
}
