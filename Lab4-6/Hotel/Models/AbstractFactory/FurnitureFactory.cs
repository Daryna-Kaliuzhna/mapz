﻿namespace Hotel.Models.AbstractFactory
{
    public abstract class FurnitureFactory
    {
        public abstract Furniture CreateChair();
        public abstract Furniture CreateTable();
        public abstract Furniture CreateSofa();
        public abstract Furniture CreateBed();
    }
}

