﻿using Hotel.Models.LuxeFurniture;

namespace Hotel.Models.AbstractFactory
{
    public class LuxeFurnitureFactory : FurnitureFactory
    {
        public override Furniture CreateChair()
        {
            return new LuxeChair();
        }
        public override Furniture CreateTable()
        {
            return new LuxeTable();
        }
        public override Furniture CreateSofa()
        {
            return new LuxeSofa();
        }
        public override Furniture CreateBed()
        {
            return new LuxeBed();
        }
    }
}
