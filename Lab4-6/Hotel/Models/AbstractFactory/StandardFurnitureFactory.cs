﻿using Hotel.Models.StandardFurniture;

namespace Hotel.Models.AbstractFactory
{
    public class StandardFurnitureFactory : FurnitureFactory
    {
        public override Furniture CreateChair()
        {
            return new StandardChair();
        }
        public override Furniture CreateTable()
        {
            return new StandardTable();
        }
        public override Furniture CreateSofa()
        {
            return new StandardSofa();
        }
        public override Furniture CreateBed()
        {
            return new StandardBed();
        }
    }
}
