﻿namespace Hotel.Models.Builders
{
    public class BuilderDirector
    {
        private IBuilder builder;

        public BuilderDirector(IBuilder builder)
        {
            this.builder = builder;
        }
        public void ConstructRoom()
        {
            builder.Reset();
            builder.AddBed();
            builder.AddChair();
            builder.AddTable();
            builder.AddSofa();
        }
    }
}
