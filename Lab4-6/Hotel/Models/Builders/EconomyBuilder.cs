﻿using Hotel.Models.EconomyFurniture;
using Hotel.Models.Flyweight;

namespace Hotel.Models.Builders
{
    public class EconomyBuilder : IBuilder
    {
        public Room room;

        public EconomyBuilder()
        {
            Reset();
        }
        public void Reset()
        {
            room = new Room { Type = "Economy", Price = 900, Image = ImageFactory.CreateEconomyRoomImage(), 
                Furniture = new List<Furniture>(),
                IsAvailable = false, Description ="",
                AdditionalFeatures = new List<string>()};
        }
        public void AddSofa()
        {
            room.Furniture.Add((new EconomySofa()));
        }

        public void AddBed()
        {
            room.Furniture.Add((new EconomyBed()));
        }

        public void AddChair()
        {
            room.Furniture.Add((new EconomyChair()));
        }

        public void AddTable()
        {
            room.Furniture.Add((new EconomyTable()));
        }
        public void SetType(string type)
        {
            room.Type = type;
        }
        public Room GetRoom()
        {
            Room result = room;
            Reset();
            return room;
        }

    }

}
