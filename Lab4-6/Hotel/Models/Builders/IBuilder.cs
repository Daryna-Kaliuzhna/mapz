﻿namespace Hotel.Models.Builders
{
    public interface IBuilder
    {
        public void AddBed();
        public void AddChair();
        public void AddSofa();
        public void AddTable();
        public Room GetRoom();
        public void Reset();

    }
}
