﻿using Hotel.Models.Flyweight;
using Hotel.Models.LuxeFurniture;

namespace Hotel.Models.Builders
{
    public class LuxeBuilder : IBuilder
    {
        public Room room;

        public LuxeBuilder()
        {
            Reset();
        }
        public void Reset()
        {
           
            room = new Room { Type = "Luxe", Price = 3500, Image = ImageFactory.CreateLuxeRoomImage(), 
                Furniture = new List<Furniture>(),
                IsAvailable = false, Description = "",
                AdditionalFeatures = new List<string>()
            };
        }
        public void AddSofa()
        {
            room.Furniture.Add((new LuxeSofa()));
        }

        public void AddBed()
        {
            room.Furniture.Add((new LuxeBed()));
        }

        public void AddChair()
        {
            room.Furniture.Add((new LuxeChair()));
        }

        public void AddTable()
        {
            room.Furniture.Add((new LuxeTable()));
        }
        public Room GetRoom()
        {
            Room result = room;
            Reset();
            return result;
        }
    }
}
