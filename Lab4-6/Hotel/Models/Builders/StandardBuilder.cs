﻿using Hotel.Models.Flyweight;
using Hotel.Models.StandardFurniture;
using Hotel.Models.State;

namespace Hotel.Models.Builders
{
    public class StandardBuilder : IBuilder
    {
        public Room room;

        public StandardBuilder()
        {
            Reset();
        }
        public void Reset()
        {
           
            room = new Room { Type = "Standard", Price = 1500, Image = ImageFactory.CreateStandardRoomImage(), 
                Furniture = new List<Furniture>(),IsAvailable = false, Description = "",
                AdditionalFeatures = new List<string>()
            };

        }
        public void AddSofa()
        {
            room.Furniture.Add((new StandardSofa()));
        }

        public void AddBed()
        {
            room.Furniture.Add((new StandardBed()));
        }

        public void AddChair()
        {
            room.Furniture.Add((new StandardChair()));
        }

        public void AddTable()
        {
            room.Furniture.Add((new StandardTable()));
        }
        
        public Room GetRoom()
        {
            Room result = room;
            Reset();
            return result;
        }

    }
}
