﻿namespace Hotel.Models.Decorator
{
    public class BreakFastDecorator : RoomDecorator
    {
        public BreakFastDecorator(Room room) : base(room) { }

        public override void SetDescription()
        {
            room.Description += " Breakfast";

        }

        public override void SetPrice()
        {
            room.Price += 200;
        }
    }
}
