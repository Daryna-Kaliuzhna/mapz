﻿namespace Hotel.Models.Decorator
{
    public class GoodViewDecorator : RoomDecorator
    {
        public GoodViewDecorator(Room room) : base(room) { }

        public override void SetDescription()
        {
            room.Description += " Good view";

        }
        public override void SetPrice()
        {
            room.Price += 100;

        }
    }
}
