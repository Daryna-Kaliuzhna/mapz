﻿namespace Hotel.Models.Decorator
{
    public abstract class RoomDecorator
    {
        protected Room room;

        public RoomDecorator(Room room)
        {
            this.room = room;
        }
        public abstract void SetDescription();
        public abstract void SetPrice();
        
    }
}
