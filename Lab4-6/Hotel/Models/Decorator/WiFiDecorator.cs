﻿namespace Hotel.Models.Decorator
{
    public class WiFiDecorator : RoomDecorator
    {
        public WiFiDecorator(Room room) : base(room) { }

        public override void  SetDescription()
        {
            room.Description += " Wi-Fi";
        }

        public override void SetPrice()
        {
            room.Price += 150;
        }
    }
}
