﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.EconomyFurniture
{
    public class EconomyBed : Bed
    {
       public EconomyBed()
            :base("Classic","Wood", 4900, "Single", false) 
       { }
    }
}
