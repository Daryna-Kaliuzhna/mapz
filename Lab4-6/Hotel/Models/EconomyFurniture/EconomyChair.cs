﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.EconomyFurniture
{
    public class EconomyChair: Chair
    {
        public EconomyChair()
            :base("Berta", "Fabric", 3000, false, false)
        { }
    }
}
