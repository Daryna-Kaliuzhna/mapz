﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.EconomyFurniture
{
    public class EconomySofa :Sofa
    {
        public EconomySofa()
            :base("Ennio", "Fabric", 9900, "Not soft", false)
        { }
    }
}
