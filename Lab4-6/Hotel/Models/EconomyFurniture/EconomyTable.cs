﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.EconomyFurniture
{
    public class EconomyTable : Table
    {
        public EconomyTable() 
            :base("T-300-11", "Playwood", 3000, "Rectangular", false)
        { }
    }
}
