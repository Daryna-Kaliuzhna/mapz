﻿using System.Xml.Linq;

namespace Hotel.Models.Facade
{
    public class GuestRegistrationSystem
    {
        public Hotel hotel = Hotel.GetInstance();
        public void RegisterGuest(string name, string idDocument)
        {
          //bool isGuestExists = hotel.Guests.Any(g => g.Name == name && g.IdDocument == idDocument);

          //  if (!isGuestExists)
          //  {
                
          //  }
          //  else
          //  {
          //      Console.WriteLine($"Guest {name} with ID document {idDocument} already exists in the hotel.");
          //  }

            Guest guest = new Guest(name, idDocument);
            hotel.Guests.Add(guest);

        }

        public void ProvideRoomKey(int roomID,  string idDocument, string roomType, DateTime startDate, DateTime endDate)
        {
            var guest = hotel.Guests.FirstOrDefault(g => g.IdDocument == idDocument);

            if (guest != null)
            {
               guest.roomID = roomID;
               guest.roomType = roomType;
               guest.startDate = startDate;
               guest.endDate = endDate;
            }
           
        }
    }
}
