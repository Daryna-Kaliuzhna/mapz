﻿namespace Hotel.Models.Facade
{
    public class HotelManagementSystem
    {
        private RoomBookingSystem roomBookingSystem;
        private GuestRegistrationSystem guestRegistrationSystem;

        public HotelManagementSystem()
        {
            roomBookingSystem = new RoomBookingSystem();
            guestRegistrationSystem = new GuestRegistrationSystem();
        }

        public void BookRoom(int roomID)
        {
            roomBookingSystem.BookRoom( roomID);
        }

        public void CancelBooking(int roomID)
        {
            roomBookingSystem.CancelBooking(roomID);
        }

        public void RegisterGuest(string name, string idDocument)
        {
            guestRegistrationSystem.RegisterGuest(name, idDocument);
        }

        public void ProvideRoomKey(int roomID, string idDocument, string roomType, DateTime startDate, DateTime endDate)
        {
            guestRegistrationSystem.ProvideRoomKey(roomID, idDocument, roomType, startDate, endDate);
        }
    }
}
