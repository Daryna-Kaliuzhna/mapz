﻿namespace Hotel.Models.Facade
{
    public class RoomBookingSystem
    {
        public Hotel hotel = Hotel.GetInstance();
        public void BookRoom(int roomID)
        {
            Room room = hotel.Rooms.FirstOrDefault(r => r.ID == roomID);

            if (!room.IsAvailable)
            {
                room.IsAvailable = true;
        
            }
        }

        public void CancelBooking(int roomID)
        {
            Room room = hotel.Rooms.FirstOrDefault(r => r.ID == roomID);

            if (room.IsAvailable)
            {
                room.IsAvailable = false;
            }
        }
    }
}
