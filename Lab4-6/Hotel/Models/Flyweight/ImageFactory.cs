﻿using System.Collections;
using System.Collections.Generic;

namespace Hotel.Models.Flyweight
{
    public class ImageFactory
    {
        public static Dictionary<string, string> images = new Dictionary<string, string>();

        public static string CreateEconomyRoomImage()
        {
            if (!images.ContainsKey("Economy"))
            {
                images["Economy"] = "http://localhost:7279/images/85.jpg";
            }
            return images["Economy"];
        }

        public static string CreateStandardRoomImage()
        {
            if (!images.ContainsKey("Standard")) 
            {
                images["Standard"] = "http://localhost:7279/images/131.jpg";
            }
            return images["Standard"];
        }

        public static string CreateLuxeRoomImage()
        {
            if (!images.ContainsKey("Luxe"))
            {
                images["Luxe"] = "http://localhost:7279/images/104.jpg";
            }
            return images["Luxe"];
        }

    }
}
