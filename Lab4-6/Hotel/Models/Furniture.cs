﻿namespace Hotel.Models
{
    public abstract class Furniture
    {
        public string Type { get;}
        public string Name { get; set; }
        public string Material { get; set; }
        public double Price { get; set; }
        public Furniture(string type, string name, string material, double price)
        {
            Type = type;
            Name = name;
            Material = material;
            Price = price;
        }
    }
}
