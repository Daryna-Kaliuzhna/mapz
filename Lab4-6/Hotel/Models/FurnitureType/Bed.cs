﻿namespace Hotel.Models.FurnitureType
{
    public class Bed : Furniture
    {
        public string Type = "Bed";
        public string Size { get; set; } 
        public bool IsAdjustable { get; set; }

        public Bed(string name, string material, double price, string size, bool isAdjustable)
            :base("Bed",name,material,price)
        {
            this.Size = size;
            this.IsAdjustable = isAdjustable;
        }
    }
}
