﻿namespace Hotel.Models.FurnitureType
{
    public class Chair : Furniture
    {
        public string Type = "Chair";
        public bool HasArmrests { get; set; }
        public bool IsReclining { get; set; }
        public Chair(string name, string material, double price, bool hasArmrests, bool isReclining)
            :base("Chair", name, material, price)
        {
            this.HasArmrests = hasArmrests;
            this.IsReclining = isReclining;
        }
    }
}
