﻿namespace Hotel.Models.FurnitureType
{
    public class Sofa : Furniture
    {
		public string Type = "Sofa";
		public string Softness { get; set; }
        public bool IsConvertible { get; set; }

        public Sofa(string name, string material, double price, string softness, bool isConvertible)
            :base("Sofa", name, material, price)
        {
            this.Softness = softness;
            this.IsConvertible = isConvertible;
        }
    }
}
