﻿namespace Hotel.Models.FurnitureType
{
    public class Table : Furniture
    {
		public string Type = "Table";
		public string Shape { get; set; }
        public bool IsExtendable { get; set; }

        public Table(string name, string material, double price, string shape, bool isExtendable)
            :base("Table", name, material, price)
        {
            this.Shape = shape;
            this.IsExtendable = isExtendable;   
        }
    }
}
