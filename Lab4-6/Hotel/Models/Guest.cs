﻿namespace Hotel.Models
{
    public class Guest
    {
        public string Name { get;}
        public string IdDocument { get;}  
        public int roomID { get; set; }
        public string roomType { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

        public Guest(string name, string isDocument)
        {
            Name = name;
            IdDocument = isDocument;
            roomID = 0;
            roomType = "";
        }
    }
}
