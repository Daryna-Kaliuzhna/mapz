﻿using Hotel.Models.Observer;

namespace Hotel.Models
{
    public class Hotel
    {
        public string Name = "GrandHotel";

        private static Hotel _instance;
       // public List<Worker> Workers { get; private set; }
        public List<Room> Rooms { get; private set; }
        public List<Furniture> Furniture { get; private set;}
        public List<Guest> Guests { get; private set; }
        public double Budget { get; set; }

        public List<string> Features { get; private set; }

        public List<IRoomObserver> roomObservers = new List<IRoomObserver>();

        private Hotel()
        {
            Budget = 1000000;
            //Workers = new List<Worker>();
            Furniture = new List<Furniture>();
            Rooms = new List<Room>();
            Guests = new List<Guest>();
            Features = new List<string>();
        }
        public static Hotel GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Hotel();
            }
            return _instance;
        }

        public void AddRoomObserver(IRoomObserver observer)
        {
            roomObservers.Add(observer);
        }

        public void NotifyRoomObservers(string feature)
        {
            foreach (var observer in roomObservers)
            {
                observer.AddFeatureToRoom(feature);
            }
        }

        public void AddAdditionalFeature(string feature)
        {
            Features.Add(feature);
            NotifyRoomObservers(feature);
        }
    }
}
