﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.LuxeFurniture
{
    public class LuxeBed : Bed
    {
        public LuxeBed()
          : base("Luxury-55", "Wood", 18000, "Double", true)
        { }
    }
}
