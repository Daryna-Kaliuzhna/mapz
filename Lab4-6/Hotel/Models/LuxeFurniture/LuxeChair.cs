﻿using Hotel.Models.FurnitureType;
namespace Hotel.Models.LuxeFurniture
{
    public class LuxeChair : Chair
    {
        public LuxeChair()
           : base("Soft-156", "Leather", 12000, true, true)
        { }
    }
}
