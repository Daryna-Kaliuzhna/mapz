﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.LuxeFurniture
{
    public class LuxeSofa : Sofa
    {
        public LuxeSofa()
          : base("Luxury sofa", "Leather", 17000, "Very soft", true)
        { }
    }
}

