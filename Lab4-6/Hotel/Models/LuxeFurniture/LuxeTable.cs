﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.LuxeFurniture
{
    public class LuxeTable : Table
    {
        public LuxeTable()
         : base("Table-310-01", "Oak", 10000, "Rectangular", true)
        { }
    }
}
