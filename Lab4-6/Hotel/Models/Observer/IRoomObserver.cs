﻿namespace Hotel.Models.Observer
{
    public interface IRoomObserver
    {
        void AddFeatureToRoom(string feature);
    }
}
