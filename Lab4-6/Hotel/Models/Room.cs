﻿using Hotel.Models.Observer;
using Hotel.Models.State;

namespace Hotel.Models
{
    public class Room : IRoomObserver
    {
        public int ID;
        public string Type { get; set;}
        public double Price {get; set;} 
        public string Image { get; set;}
        public string Description { get; set;}
        public List<Furniture> Furniture { get; set; }
        public IRoomState State { get; set; }
        public string StateInfo { get; set; }
        public bool IsAvailable { get; set;}

        public List<string> AdditionalFeatures { get; set; }

        public void UpdateState(IRoomState newState)
        {
            State = newState;
        }

        public void ChangeState() {

            State.Handle(this);
        }

        public void AddFeatureToRoom(string feature)
        {
            AdditionalFeatures.Add(feature);
        }
    }
}
