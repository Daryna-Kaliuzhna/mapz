﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.StandardFurniture
{
    public class StandardBed : Bed
    {
        public StandardBed()
           : base("Comfort-1506", "Wood", 8900, "Single", true)
        { }
    }
}
