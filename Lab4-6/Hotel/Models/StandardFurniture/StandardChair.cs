﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.StandardFurniture
{
    public class StandardChair : Chair
    {
        public StandardChair()
           : base("Relax-78", "Imitation leather", 8000, true, false)
        { }
    }
}
