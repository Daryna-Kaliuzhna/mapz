﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.StandardFurniture
{
    public class StandardSofa : Sofa
    {
        public StandardSofa()
          : base("Sofi", "Imitation leather", 15000, "Not very soft", true)
        { }
    }
}
