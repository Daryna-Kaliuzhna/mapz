﻿using Hotel.Models.FurnitureType;

namespace Hotel.Models.StandardFurniture
{
    public class StandardTable : Table
    {
        public StandardTable()
           : base("T-256-12", "Wood", 7000, "Round", true)
        { }
    }
}
