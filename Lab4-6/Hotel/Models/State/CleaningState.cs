﻿namespace Hotel.Models.State
{
    public class CleaningState : IRoomState
    {
        public void Handle(Room room)
        {
            room.StateInfo = "is being cleaned ";
            room.IsAvailable = false;
        }
    }
}
