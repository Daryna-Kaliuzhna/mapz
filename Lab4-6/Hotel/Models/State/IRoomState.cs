﻿namespace Hotel.Models.State
{
    public interface IRoomState
    {
        void Handle(Room room); 
    }
}
