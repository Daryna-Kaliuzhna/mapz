﻿namespace Hotel.Models.State
{
    public class RepairState : IRoomState
    {
        public void Handle(Room room)
        {
            room.StateInfo = "is being repaired";
            room.IsAvailable = false;
        }
    }
}
