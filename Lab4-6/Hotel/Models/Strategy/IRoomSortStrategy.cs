﻿namespace Hotel.Models.Strategy
{
    public interface IRoomSortStrategy
    {
        void SortRooms(List<Room> rooms);
    }
}
