﻿namespace Hotel.Models.Strategy
{
    public class RoomSortManager
    {
        private IRoomSortStrategy _sortStrategy;

        public RoomSortManager(IRoomSortStrategy sortStrategy)
        {
            _sortStrategy = sortStrategy;
        }

        public void SortRooms(List<Room> rooms)
        {
            _sortStrategy.SortRooms(rooms);
        }
    }
}
