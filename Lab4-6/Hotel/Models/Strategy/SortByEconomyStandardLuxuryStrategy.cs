﻿namespace Hotel.Models.Strategy
{
    public class SortByEconomyStandardLuxuryStrategy : IRoomSortStrategy
    {
        public void SortRooms(List<Room> rooms)
        {
            rooms.Sort((x, y) =>
            {
                if (x.Type == "Economy" && y.Type != "Economy")
                    return -1;
                else if (x.Type != "Economy" && y.Type == "Economy")
                    return 1;
                else if (x.Type == "Standard" && y.Type != "Standard")
                    return -1;
                else if (x.Type != "Standard" && y.Type == "Standard")
                    return 1;
                else
                    return 0;
            });
        }
    }
}

