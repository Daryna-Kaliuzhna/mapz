﻿namespace Hotel.Models.Strategy
{
    public class SortByLuxuryStandardEconomyStrategy : IRoomSortStrategy
    {
        public void SortRooms(List<Room> rooms)
        {
            rooms.Sort((x, y) =>
            {
                if (x.Type == "Luxe" && y.Type != "Luxe")
                    return -1;
                else if (x.Type != "Luxe" && y.Type == "Luxe")
                    return 1;
                else if (x.Type == "Standard" && y.Type != "Standard")
                    return -1;
                else if (x.Type != "Standard" && y.Type == "Standard")
                    return 1;
                else
                    return 0;
            });
        }
    }
}
