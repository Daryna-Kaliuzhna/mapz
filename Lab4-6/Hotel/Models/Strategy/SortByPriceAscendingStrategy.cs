﻿namespace Hotel.Models.Strategy
{
    public class SortByPriceAscendingStrategy : IRoomSortStrategy
    {
        public void SortRooms(List<Room> rooms)
        {
            rooms.Sort((x, y) => x.Price.CompareTo(y.Price));
        }
    }
}
