﻿namespace Hotel.Models.Strategy
{
    public class SortByPriceDescendingStrategy : IRoomSortStrategy
    {
        public void SortRooms(List<Room> rooms)
        {
            rooms.Sort((x, y) => y.Price.CompareTo(x.Price));
        }
    }
}
