﻿
function showFurniture() {
    var shopSelect = document.getElementById("shopSelect");
    var selectedShop = shopSelect.value;

    document.getElementById("furnitureList").style.display = "block";
    document.getElementById("economyFurniture").style.display = "none";
    document.getElementById("standardFurniture").style.display = "none";
    document.getElementById("luxeFurniture").style.display = "none";

    if (selectedShop === "economy") {
        document.getElementById("economyFurniture").style.display = "block";
    } else if (selectedShop === "standard") {
        document.getElementById("standardFurniture").style.display = "block";
    } else if (selectedShop === "luxe") {
        document.getElementById("luxeFurniture").style.display = "block";
    }
}
   
